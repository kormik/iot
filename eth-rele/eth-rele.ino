#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>

const int RELE1_PIN = 6;
const int RELE2_PIN = 7;
const int RELE3_PIN = 8;
const int RELE4_PIN = 9;

byte mac[] = {0xCC, 0xFA, 0x06, 0xCB, 0x19, 0x00};

// Arduino IP
IPAddress ip(10, 0, 0, 10);

// MQTT broker IP
byte server[] = {10, 0, 0, 1};

void callback(char *topic, byte *payload, unsigned int length) {
    String content = "";
    char character;
    for (int num = 0; num < length; num++) {
        character = payload[num];
        content.concat(character);
    }
    if (content == "RELE1_ON") { digitalWrite(RELE1_PIN, LOW); }
    if (content == "RELE1_OFF") { digitalWrite(RELE1_PIN, HIGH); }
    if (content == "RELE2_ON") { digitalWrite(RELE2_PIN, LOW); }
    if (content == "RELE2_OFF") { digitalWrite(RELE2_PIN, HIGH); }
    if (content == "RELE3_ON") { digitalWrite(RELE3_PIN, LOW); }
    if (content == "RELE3_OFF") { digitalWrite(RELE3_PIN, HIGH); }
    if (content == "RELE4_ON") { digitalWrite(RELE4_PIN, LOW); }
    if (content == "RELE4_OFF") { digitalWrite(RELE4_PIN, HIGH); }
}

EthernetClient arduino0;
PubSubClient client(server, 1883, callback, arduino0);

void setup() {
    digitalWrite(RELE1_PIN, HIGH);
    pinMode(RELE1_PIN, OUTPUT);
    digitalWrite(RELE2_PIN, HIGH);
    pinMode(RELE2_PIN, OUTPUT);
    digitalWrite(RELE3_PIN, HIGH);
    pinMode(RELE3_PIN, OUTPUT);
    digitalWrite(RELE4_PIN, HIGH);
    pinMode(RELE4_PIN, OUTPUT);

    Ethernet.begin(mac, ip);
}

long lastReconnectAttempt = 0;

boolean connect() {
    if (client.connect("arduino", "test", "test")) {
        client.publish("arduino/status", "ON");
        Serial.println("mqtt connected");
        client.subscribe("arduino/command");
    }
    return client.connected();
}

void loop() {
    if (!client.connected()) {
        long now = millis();
        if (now - lastReconnectAttempt > 5000) {
            lastReconnectAttempt = now;
            if (connect()) {
                lastReconnectAttempt = 0;
            }
        }
    } else {
        client.loop();
    }
}
